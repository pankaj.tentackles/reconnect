var gulp = require("gulp");
var gutil = require("gulp-util");
var shell = require('gulp-shell');
var webpack = require("webpack");
var runSequence = require('run-sequence');
var WebpackDevServer = require("webpack-dev-server");
var webpackConfig = require("./webpack.config.js");
var spawn = require('child_process').spawn;
var NwBuilder = require('nw-builder');
var webpackConfigProd = require("./config/webpack.prod.js");
var rimraf = require('rimraf'); // rimraf directly 


// The development server (the recommended option for development)
gulp.task("default", ["webpack-dev-server"]);


gulp.task('rimraf:dist', function (cb) {
   rimraf('./dist',cb);
});

gulp.task('ShellTask', shell.task([
    // 'rm -rf dist',
    'mkdir dist'
]));


// modify some webpack config options


gulp.task("webpack:build:dev", function(callback) {
    var myDevConfig = Object.create(webpackConfig);
    myDevConfig.entry.livereload = './src/livereload.js';
    myDevConfig.target = 'node-webkit';
    // create a single instance of the compiler to allow caching
    var devCompiler = webpack(myDevConfig);
    // run webpack
    devCompiler.run(function(err, stats) {
        if (err) throw new gutil.PluginError("webpack:build:dev", err);
        gutil.log("[webpack:build:dev]", stats.toString({
            colors: true
        }));
        callback();
    });
});

// Production build
gulp.task('build', function(callback) {
    runSequence(
        'rimraf:dist',
        'ShellTask',
        'webpack:build:prod',
        callback);
});

// gulp.task('webpack:build:prod', shell.task([
//     'webpack --config config/webpack.prod.js --progress --profile --bail'
// ]));

gulp.task("webpack:build:prod", function(callback) {
    // modify some webpack config options
    var myConfig = Object.create(webpackConfigProd);
   
    // run webpack
    webpack(myConfig, function(err, stats) {
        if (err) throw new gutil.PluginError("webpack:build", err);
        gutil.log("[webpack:build]", stats.toString({
            colors: true
        }));
        callback();
    });
});


gulp.task("webpack-dev-server", function(callback) {
    // modify some webpack config options
    var myConfig = Object.create(webpackConfig);
   
    // Start a webpack-dev-server
    new WebpackDevServer(webpack(myConfig), {
        publicPath: "/" + myConfig.output.publicPath,
        stats: {
            colors: true
        }
    }).listen(8080, "localhost", function(err) {
        if (err) throw new gutil.PluginError("webpack-dev-server", err);
        gutil.log("[webpack-dev-server]", "http://localhost:8080/");
    });
});
