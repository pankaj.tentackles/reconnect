const path = require('path');
const webpack = require('webpack');
const onceImporter = require('node-sass-once-importer');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin'); //installed via npm
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const {pageEntries, pageIndexHTMLS, pageScssLoaders} = require('./config-helper');

// let commonsPlugin = new webpack.optimize.CommonsChunkPlugin(
//     'commons',  // Just name it
//     'common.js' // Name of the output file
//                 // There are more options, but we don't need them yet.
// );

const buildPath = path.resolve(__dirname, '../dist');

module.exports = {
    devtool: 'source-map',
    entry: pageEntries,
    output: {
        filename: `js/[name].[hash:20].js`,
        path: buildPath
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                options: {
                    presets: ['env']
                }
            },
            ...pageScssLoaders.map(({ test, plugin }) => {
                return {
                    // test: /\.(scss|css|sass)$/,
                    test,
                    use: plugin.extract({
                        use: [
                            {
                                // translates CSS into CommonJS
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                // Runs compiled CSS through postcss for vendor prefixing
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                // compiles Sass to CSS
                                loader: 'sass-loader',
                                options: {
                                    outputStyle: 'expanded',
                                    sourceMap: true,
                                    sourceMapContents: true
                                }
                            }
                        ],
                        fallback: 'style-loader'
                    }),
                }
            }),
            {
                test: /\.(css)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            // translates CSS into CommonJS
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // Runs compiled CSS through postcss for vendor prefixing
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // compiles Sass to CSS
                            loader: 'sass-loader',
                            options: {
                                outputStyle: 'expanded',
                                sourceMap: true,
                                sourceMapContents: true
                            }
                        }
                    ],
                    fallback: 'style-loader'
                }),
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: ['video:poster', 'img:src', 'link:href', 'source:src']
                      }
                  }
            },
            {
                test: /\.woff(2)?(\?[a-z0-9]+)?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            }, 
            {
                test: /\.(ttf|eot|svg)(\?[a-z0-9]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[name].[ext]',
                            outputPath: '/css/fonts',
                            // useRelativePath: true
                        }
                    }
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|jpeg|gif|pdf|svg|mp4|webm)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[name].[ext]?hash=[hash:20]',
                            outputPath: '/assets',
                            publicPath: '/assets'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        ...pageIndexHTMLS,
        new CleanWebpackPlugin(buildPath),
        new FaviconsWebpackPlugin({
            // Your source logo
            logo: './src/assets/icon.png',
            // The prefix for all image files (might be a folder or a name)
            prefix: '/favicons-[hash]/',
            // Generate a cache file with control hashes and
            // don't rebuild the favicons until those hashes change
            persistentCache: true,
            // Inject the html into the html-webpack-plugin
            inject: true,
            // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
            background: '#fff',
            // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
            title: '{{projectName}}',

            // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
            icons: {
                android: true,
                appleIcon: true,
                appleStartup: true,
                coast: false,
                favicons: true,
                firefox: true,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
            }
        }),
        ...pageScssLoaders.map(scss=>scss.plugin),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                map: {
                    inline: false,
                },
                discardComments: {
                    removeAll: true
                }
            },
            canPrint: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": "jquery",
            WOW:"wow.js",
            "window.WOW": "wow.js"
          })          
    ],
    resolve: {
        alias: {
            common: path.resolve(__dirname, '../src/common/'),
            assets: path.resolve(__dirname, '../src/assets/')
        }
    }
};
