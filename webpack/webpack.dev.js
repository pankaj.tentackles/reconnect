const path = require('path');
const webpack = require('webpack');
const combineLoaders = require('webpack-combine-loaders');
const { pageEntries, pageIndexHTMLS } = require('./config-helper');
// const plugins = [...pageIndexHTMLS.map(html=>html.plugin)];

// console.log(plugins);

module.exports = {
    devtool: 'eval-cheap-module-source-map',
    entry: pageEntries,
    devServer: {
        port: 8080,
        overlay: true,
        watchContentBase: true,                 
        contentBase: path.join(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: "sass-loader",
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: true,
                            sourceMapContents: true
                        }
                    }
                    // Please note we are not running postcss here
                ]
            },
            
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: ['video:poster', 'img:src', 'link:href', 'source:src']
                      }
                  }
            },
            {
                test: /\.woff(2)?(\?[a-z0-9]+)?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            }, 
            {
                test: /\.(ttf|eot|svg)(\?[a-z0-9]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                            useRelativePath: true
                        }
                    }
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|jpeg|gif|pdf|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[name].[ext]?hash=[hash:20]',
                            outputPath: 'img',
                            // outputPath: 'video',
                            useRelativePath: true
                        }
                    }
                ]
            },
            {
                test: /\.(mov|mp4|webm|ogg)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                      name: '[name].[ext]?hash=[hash:20]',
                      useRelativePath: true
                    }  
                  }
                ]
              }
        ],
    },




    
    plugins: [
        ...pageIndexHTMLS,
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": "jquery",
            WOW:"wow.js",
            "window.WOW": "wow.js",

            // fullpage: "fullpage.js",
            // "window.fullpage.js": "fullpage.js",
            
            
            
        })

    ],
    resolve: {
        alias: {
            common: path.resolve(__dirname, '../src/common/')
        }
    }
};
