const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const globby = require('globby');

const sections = globby.sync(['src/**/page-manifeast.js', '!src/**/js/**', '!src/**/styles/**', '!src/**/assets/**'], {
    onlyFiles: true
});

const bundles = sections.map(section => require(`../${section}`));

const pageEntries = bundles.reduce((agg = {}, {entry, pageName}) => {
    Object.assign(agg, {[pageName]: entry});
    return agg;
}, {});

const pageIndexHTMLS = bundles.map(({ indexHTML, pageName, landing }) => {
    return new HtmlWebpackPlugin({
        template: indexHTML,
        filename: landing ? `index.html` : `${pageName}/index.html`,
        chunks:[pageName],
        // Inject the js bundle at the end of the body of the given template
        inject: 'body'
    })
});

const pageScssLoaders = bundles.map(({ scssEntry, pageName }) => {
    return {
        test: scssEntry,
        plugin: new ExtractTextPlugin(`css/${pageName}.[md5:contenthash:hex:20].css`, {
            allChunks: true
        }),
    }
});

module.exports = {
    pageEntries,
    pageIndexHTMLS,
    pageScssLoaders
}