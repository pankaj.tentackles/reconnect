require('../styles/index.scss');

//common js should be imported here
import 'common/js/header.js'
import 'wow.js';

//page specific js should be imported here
import './services.js'


//No js code should be written in this file only imports.