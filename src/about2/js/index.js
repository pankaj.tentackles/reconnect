
require('../styles/index.scss');
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';

import 'bootstrap';
import 'owl.carousel';
//import 'fullpage.js';
import 'wow.js';


//common js should be imported here
import 'common/js/header.js';

//page specific js should be imported here
import './about2.js'


//No js code should be written in this file only imports.