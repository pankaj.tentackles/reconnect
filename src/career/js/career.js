// $(function(){
//     document.onload = () => new WOW().init(); 
//   });

// Page specific JS written here

//import fullpage from 'fullpage.js';

$(document).ready(e => {
    $('#carousel').owlCarousel({
        loop: true,
        // margin: 10,
        items: 3,
        responsiveBaseElement: '#carousel',
        responsiveRefreshRate: '200',
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        nav: true,

        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
   
   
});


$(document).ready(e => {
    $('#carousel2').owlCarousel({
        loop: true,
        // margin: 10,
        items: 1,
        responsiveBaseElement: '#carousel2',
        responsiveRefreshRate: '180',
        margin:20,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            // 1000: {
            //     items: 3
            // }
        }
    });
   
   
});


$('#carouselIndicators').on('slide.bs.carousel', function () {
    // do something…
  })


//   $(document).ready(function() {
//     onload = () => new WOW().init();
//     });
//   const wow = new WOW({
//     boxClass: 'wow',
//     animateClass: 'animated',
//     offset: 0,
//     live: true
//   });

$(document).ready(function() {
    new WOW().init();
    });


    // $(document).ready(function() {
    //     $('#fullpage').fullpage({
    //         scrollBar: true,
    //         afterRender: function(){
    //               new WOW().init();
    //         }
    //     });
    // });

   $(document).ready(function(){
       $(".opennav").click(function(){
        $(".navopen").show();
       });
       $(".navclose").click(function(){
        $(".rightpanel").hide();
       });
   });



    // new fullpage('#fullpage',{
    //     scrollBar: true,
    //     // responsiveWidth: 1100,
    //     fadingEffectKey: 'dmVydGl2ZXIuY29tXzA3VWMyTnliMnhzU0c5eWFYcHZiblJoYkd4NVU4Vw',
        
    // });




// $(function(){
//     document.onload = () => new WOW().init(); 
//   });

// Page specific JS written here

//import fullpage from 'fullpage.js';



$(document).ready(function() {
    new WOW().init();
    });


    // $(document).ready(function() {
    //     $('#fullpage').fullpage({
    //         scrollBar: true,
    //         afterRender: function(){
    //               new WOW().init();
    //         }
    //     });
    // });

   $(document).ready(function(){
       $(".opennav").click(function(){
        $(".navopen").show();
       });
       $(".navclose").click(function(){
        $(".rightpanel").hide();
       });
   });



    // new fullpage('#fullpage',{
    //     scrollBar: true,
    //     // responsiveWidth: 1100,
    //     fadingEffectKey: 'dmVydGl2ZXIuY29tXzA3VWMyTnliMnhzU0c5eWFYcHZiblJoYkd4NVU4Vw',
        
    // });


    $(document).ready(function () {
        $('#attachment').on('submit',function(event){
            event.preventDefault();
            var _self = this;
            $(_self).find(".result").html('<center style="margin-top:35px;"><img src="../assets/progress-bar.gif" /></center>');
            $.ajax({
                type: "post",
                url: "attachement.php",
                enctype: 'multipart/form-data',
                data: new FormData(this),
                cache: false,
                data: $(_self).serialize(),
                success: function (json) {
                    try {
                        var obj = jQuery.parseJSON(json);
                        _self.reset();
                        if (obj['status']) {
                            $(_self).find(".result").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Thank you for your interest</strong>.<br>Our team will contact you shortly</div>');
                        } else {
                            $(_self).find(".result").html('<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sorry!</strong> Unable to send message. Please try again!</div>');
                        }
    
                    } catch (e) {
                        $(_self).find(".result").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Thank you for your interest</strong>.<br>Our team will contact you shortly</div>');
                    }
                },
                error: function () {
                    $(_self).find(".result").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Thank you for your interest</strong>.<br>Our team will contact you shortly</div>');
                
                    _self.reset();
                }
    
                
            });
        })
       
    });
  
