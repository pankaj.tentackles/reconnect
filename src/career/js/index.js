require('../styles/index.scss');
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';

//common js should be imported here
import 'owl.carousel';
import 'common/js/header.js'
import 'wow.js';
import './career.js'


//No js code should be written in this file only imports.