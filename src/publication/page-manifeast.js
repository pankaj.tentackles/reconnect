const path = require('path');

const manifeast = {
    pageName: "publication",
    scssEntry: path.resolve(__dirname, './styles/index.scss'),
    entry: path.resolve(__dirname, './js/index.js'),
    indexHTML: path.resolve(__dirname, './index.html')
}

module.exports = manifeast;