// const plop = require('plop');

module.exports = function (plop) {
    // create your generators here
    plop.setGenerator('basics', {
        description: '***** Tentackles Page Generator *****',
        prompts: [{
            type: 'input',
            name: 'pageName',
            message: 'Please Enter Page Name..? '
        }], // array of inquirer prompts
        actions: function(){
            return [
                addFile('index.html'),
                addFile('index.js', 'js'),
                addFile('index.scss', 'styles'),
                addFile('page-manifeast.js'),
                {
                    type: 'add',
                    path: `../src/{{dashCase pageName}}/js/{{dashCase pageName}}.js`,
                    templateFile: `templates/pageName.js.hbs`
                },
                {
                    type: 'add',
                    path: `../src/{{dashCase pageName}}/styles/{{dashCase pageName}}.scss`,
                    templateFile: `templates/pageName.scss.hbs`
                }
            ];
        }
    });
};

function addFile(file, type){
    return {
        type: 'add',
        path: `../src/{{dashCase pageName}}/${type ? `${type}/${file}` : file}`,
        templateFile: `templates/${file}.hbs`
    }
}